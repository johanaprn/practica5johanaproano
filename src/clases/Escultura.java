package clases;

import java.time.LocalDate;

public class Escultura extends Obra {

	// Sub CLASE 2
	// Atributos
	// Modificador de visibilidad

	/**
	 * Atributos de la clase escultura
	 */
	String material;
	double peso;

	/**
	 * Constructores
	 */
	// Constructores usando SuperClase
	public Escultura() {
		super();
		this.material = "";
		this.peso = 0;

	}

	/**
	 * Constructores con dos parametros Atributo de la clase Obra
	 * 
	 * @param nombreObra
	 * @param movimiento
	 */

	public Escultura(String nombreObra, String movimiento) {
		super(nombreObra, movimiento);

	}

	/**
	 * Cosntructor con dos parametros Atributos de la clase escultura
	 * 
	 * @param material
	 * @param peso
	 */
	public Escultura(String material, double peso) {
		super();
		this.material = material;
		this.peso = peso;
	}

	/**
	 * Constructor con todos los parametros de la clase Obra
	 * 
	 * @param nombreObra
	 * @param movimiento
	 * @param fechaCreacion
	 * @param precio
	 * @param vendido
	 */
	public Escultura(String nombreObra, String movimiento, LocalDate fechaCreacion, double precio, boolean vendido) {
		super(nombreObra, movimiento, fechaCreacion, precio, vendido);

	}

	/**
	 * Setters y getters de los atributos de la clase Escutura
	 * 
	 * @return
	 */
	// Setters y Getters
	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	@Override
	public String toString() {
		return "Escultura [material=" + material + ", peso=" + peso + ", toString()=" + super.toString() + "]";
	}

	/**
	 * Metodo que se utiliza para nombrar una obra por su nombre y su peso en lugar
	 * de utilizar el toString
	 */
	public void mostrarPeso() {
		System.out.println("Nombre: " + this.nombreObra);
		System.out.println("Peso: " + this.peso);
		System.out.println("______________________________");
	}
	// Metodo propio Super Clase2 u atributo propiio de SubClase2

	/**
	 * Metodo para poder modificar el peso de la escultura
	 * 
	 * @param embalaje
	 * @returnnos devuelve ya el peso de la obra junto con el peso del embalaje
	 */
	public double cambioPeso(double embalaje) {
		peso = peso + embalaje;
		return peso;

	}

}
