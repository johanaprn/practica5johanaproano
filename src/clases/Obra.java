package clases;

import java.time.LocalDate;

/**
 * @author Johana
 *
 */
public class Obra {
	// SUPERCLASE
	// Atributos
	// En este caso lo atributo son publicos para que los hijos de esta clase puedan
	// utilizar los atibutos de esta

	String nombreObra;
	String movimiento;
	LocalDate FechaCreacion;
	double precio;
	boolean vendido;
	Artista autor;

	// Constructores
	/**
	 * Constructor de Obra
	 */
	public Obra() {
		this.nombreObra = "";
		this.movimiento = "";
		this.precio = 0;
	}

	/**
	 * Constructor con dos parametros
	 * 
	 * @param nombreObra
	 * @param movimiento
	 */
	public Obra(String nombreObra, String movimiento) {
		this.nombreObra = nombreObra;
		this.movimiento = movimiento;

	}

	/**
	 * Constructor con todos los parametros de la clase Obra
	 * 
	 * @param nombreObra
	 * @param movimiento
	 * @param fechaCreacion
	 * @param precio
	 * @param vendido
	 */
	public Obra(String nombreObra, String movimiento, LocalDate fechaCreacion, double precio, boolean vendido) {
		this.nombreObra = nombreObra;
		this.movimiento = movimiento;
		FechaCreacion = fechaCreacion;
		this.precio = precio;
		this.vendido = vendido;

	}

	public Obra(String nombreObra, String movimiento, LocalDate fechaCreacion, double precio, boolean vendido,
			Artista autor) {
		super();
		this.nombreObra = nombreObra;
		this.movimiento = movimiento;
		FechaCreacion = fechaCreacion;
		this.precio = precio;
		this.vendido = vendido;
		this.autor = autor;
	}

	/**
	 * setters y getters de todos los atributos de la clase Obra
	 * 
	 * @return
	 */
	// Setters y Getters
	public String getNombreObra() {
		return nombreObra;
	}

	public void setNombreObra(String nombreObra) {
		this.nombreObra = nombreObra;
	}

	public String getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}

	public LocalDate getFechaCreacion() {
		return FechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public boolean isVendido() {
		return vendido;
	}

	public void setVendido(boolean vendido) {
		this.vendido = vendido;
	}

	public Artista getAutor() {
		return autor;
	}

	public void setAutor(Artista autor) {
		this.autor = autor;
	}

	/**
	 * Metodo to String
	 */
	@Override
	public String toString() {
		return "Obra [nombreObra=" + nombreObra + ", movimiento=" + movimiento + ", FechaCreacion=" + FechaCreacion
				+ ", precio=" + precio + (vendido ? ", vendido" : ", no vendido") + ", autor=" + autor + "]";
	}

	// Metodo propio de Super Clase

	/**
	 * Metodo para realizar un cambio al precio por aumento al que el cliente
	 * accedera dpendiendo de la temporada en que nos encontremos
	 * 
	 * @param descuento sera la cantidad que aumentemos
	 * @return nos devolvera el precio ya modificado tra aplicarle el aumento
	 */
	public double cambioPrecio(double aumento) {
		precio = -aumento;
		return precio;
	}
}
