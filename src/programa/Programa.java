package programa;

import clases.Datos;
import clases.GaleriaArte;

public class Programa {

	public static void main(String[] args) {
		System.out.println("�Bienvenido a nuestra galeria de arte�");
		GaleriaArte galeria = new GaleriaArte();
		Datos.datosartistas(galeria);
		Datos.datosObras(galeria);
		Datos.datosCuadros(galeria);
		Datos.datosEscultura(galeria);

		// MENU
		int opcion;
		do {

			Menus.menuPrincipal();
			opcion = Comprobaciones.comprobarNumero();

			switch (opcion) {
			case 1:
				Menus.menuObra(opcion, galeria);
				break;

			case 2:
				Menus.menuArtista(opcion, galeria);
				break;

			case 3:
				System.out.println("Hasta la pr�xima");
				break;

			default:
				System.out.println("Pruebe otra vez");
				break;
			}

		} while (opcion != 3);

	}

}
