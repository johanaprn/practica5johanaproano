package programa;

import java.util.Scanner;

public class Comprobaciones {
	static Scanner input = new Scanner(System.in);

	/**
	 * Este metodo creamos un try catch para controlar los errores que puede ocurrir
	 * al introducir un dato por teclado.
	 * 
	 * @return
	 */
	public static int comprobarNumero() {
		int opcion = 0;
		boolean error = true;
		while (error == true) {
			error = false;
			try {
				opcion = input.nextInt();
			} catch (java.util.InputMismatchException e) {
				System.out.println("No es un n�mero " + e);
				System.out.println("Try again");
				input.nextLine();
				error = true;
			}
		}
		return opcion;
	}
}
