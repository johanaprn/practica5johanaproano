package clases;

import java.time.LocalDate;

public class Artista {
	// CLASE 1
	// Atributos

	/**
	 * Declaracion de los atributos de la clase Artista
	 */
	private String nombre;
	private String nArtista;
	private int nObras;
	private LocalDate fechaNacimiento;
	// Constructores

	/**
	 * Constructores de Artista vacio
	 */
	public Artista() {
		this.nombre = "";
		this.nArtista = "";
		this.nObras = 0;
	}

	/**
	 * Constructro con nombre y numero del artista
	 * 
	 * @param nombre
	 * @param nArtista
	 */
	public Artista(String nombre, String nArtista) {
		this.nombre = nombre;
		this.nArtista = nArtista;
	}

	/**
	 * @param nombre
	 * @param nArtista
	 * @param nObras
	 * @param fechaNacimiento
	 */
	public Artista(String nombre, String nArtista, int nObras, LocalDate fechaNacimiento) {

		this.nombre = nombre;
		this.nArtista = nArtista;
		this.nObras = nObras;
		this.fechaNacimiento = fechaNacimiento;
	}

	// Setters y Getters

	/**
	 * Setters y Getters de cada atributo
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setnObras(int nObras) {
		this.nObras = nObras;
	}

	public void setnArtista(String nArtista) {
		this.nArtista = nArtista;
	}

	public String getnArtista() {
		return nArtista;
	}

	public int getnObras() {
		return nObras;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	/**
	 * Metodo toString
	 */
	@Override
	public String toString() {
		return "Autor [nombre=" + nombre + ", nArtista=" + nArtista + ",nObras=" + nObras + ", fechaNacimiento="
				+ fechaNacimiento + "]";
	}

	/**
	 * Metodo mostrarDatosArtista que nos devuelve solo nombre y el numero del
	 * artista Para poder ver mejor los cambios que realizamos
	 */
	public void mostrarDatosArtista() {

		System.out.println("nombre :" + this.nombre);
		System.out.println("Numero de Artista: " + this.nArtista);
		System.out.println("");
	}
}
