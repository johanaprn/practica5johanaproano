package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * @author Johana
 *
 */
public class GaleriaArte {
	static Scanner input = new Scanner(System.in);

	// Atributos: ArrayList que se consideren necesarios.

	/**
	 * declaracion de los array list
	 */
	private ArrayList<Artista> listaArtistas;
	private ArrayList<Obra> listaObras;
	private ArrayList<Cuadro> listaCuadros;
	private ArrayList<Escultura> listaEscultura;
	// Un �nico constructor: para inicializar.

	/**
	 * Constructor para inicializar
	 */
	public GaleriaArte() {
		this.listaArtistas = new ArrayList<Artista>();
		this.listaObras = new ArrayList<Obra>();
		this.listaCuadros = new ArrayList<Cuadro>();
		this.listaEscultura = new ArrayList<Escultura>();
	}

	// M�todos alta
	/**
	 * Metodo de alta de una obra
	 * 
	 * @param nombreObra
	 * @param movimiento
	 * @param fechaCreacion
	 * @param precio
	 * @param vendido
	 */
	public void altaObra(String nombreObra, String movimiento, String fechaCreacion, double precio, boolean vendido) {
		Obra nuevaObra = new Obra(nombreObra, movimiento);
		nuevaObra.setPrecio(precio);
		nuevaObra.setVendido(vendido);
		nuevaObra.setFechaCreacion(LocalDate.parse(fechaCreacion));
		listaObras.add(nuevaObra);

	}

	/**
	 * Metodo para dar de alta Artistas
	 * 
	 * @param nombre
	 * @param nArtista
	 * @param nObras
	 * @param fechaNacimiento
	 */
	public void altaArtista(String nombre, String nArtista, int nObras, String fechaNacimiento) {
		Artista nuevoArtista = new Artista(nombre, nArtista);
		nuevoArtista.setnObras(nObras);
		nuevoArtista.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
		listaArtistas.add(nuevoArtista);

	}

	/**
	 * Metodo para dar de alta Cuadros
	 * 
	 * @param nombreObra
	 * @param movimiento
	 * @param fechaCreacion
	 * @param precio
	 * @param vendido
	 * @param tecnica
	 */
	public void altaCuadro(String nombreObra, String movimiento, String fechaCreacion, double precio, boolean vendido,
			String tecnica) {

		Cuadro nuevoCuadro = new Cuadro(tecnica);
		nuevoCuadro.setNombreObra(nombreObra);
		nuevoCuadro.setMovimiento(movimiento);
		nuevoCuadro.setPrecio(precio);
		nuevoCuadro.setVendido(vendido);
		nuevoCuadro.setFechaCreacion(LocalDate.parse(fechaCreacion));

		listaCuadros.add(nuevoCuadro);
	}

	/**
	 * Metodo para dar de alta una escultura
	 * 
	 * @param nombreObra
	 * @param movimiento
	 * @param fechaCreacion
	 * @param precio
	 * @param vendido
	 * @param material
	 * @param peso
	 */
	public void altaEscultura(String nombreObra, String movimiento, String fechaCreacion, double precio,
			boolean vendido, String material, double peso) {

		Escultura nuevaEscultura = new Escultura(material, peso);
		nuevaEscultura.setNombreObra(nombreObra);
		nuevaEscultura.setMovimiento(movimiento);
		nuevaEscultura.setFechaCreacion(LocalDate.parse(fechaCreacion));

		listaEscultura.add(nuevaEscultura);

	}

	// METODOS LISTAR

	/**
	 * Metodo para listar Obras
	 */
	public void listarObra() {
		for (Obra obras : listaObras) {
			if (obras != null) {
				System.out.println(obras);
			}
		}

	}

	/**
	 * Metodo para listar artistas
	 */
	public void listarArtista() {
		for (Artista artistas : listaArtistas) {
			if (artistas != null) {
				System.out.println(artistas);
			}
		}
	}

	/**
	 * Metodo para listar artistas por numero
	 */
	public void listarArtistaNumero() {
		for (Artista artistas : listaArtistas) {
			if (artistas != null) {
				artistas.mostrarDatosArtista();
			}
		}
	}

	/**
	 * Metodo para listar cuadros En este caso realizamos el listar cuadro con un
	 * for por variar El resultado es el mismo
	 */

	public void listarCuadros() {
		for (int i = 0; i < listaCuadros.size(); i++) {
			System.out.println(listaCuadros.get(i));
		}
	}

	/**
	 * Metodo para listar esculturas
	 */
	public void listarEscultura() {
		for (Escultura esculturas : listaEscultura) {
			if (esculturas != null) {
				System.out.println(esculturas);
			}
		}
	}

	/**
	 * Metodo para listar esculturas por peso
	 */
	public void listarPesoEscultura() {
		for (Escultura escultura : listaEscultura) {
			if (escultura != null) {
				escultura.mostrarPeso();

			}

		}
	}

	// METODOS ELIMINAR

	/**
	 * metodo para eliminar con Iterator una Obra indicando su nombre
	 * 
	 * @param nombreObra
	 */
	public void eliminarObra(String nombreObra) {
		Iterator<Obra> iteradorObra = listaObras.iterator();
		while (iteradorObra.hasNext()) {
			Obra obras = iteradorObra.next();
			if (obras.getNombreObra().equals(nombreObra)) {
				iteradorObra.remove();
			}

		}
	}

	/**
	 * Metodo para eliminar un artista segun su numero
	 * 
	 * @param nArtista
	 */
	public void eliminarArtista(String nArtista) {
		Iterator<Artista> iteradorArtista = listaArtistas.iterator();
		while (iteradorArtista.hasNext()) {
			Artista artista = iteradorArtista.next();
			if (artista.getnArtista().equals(nArtista)) {
				iteradorArtista.remove();
			}

		}
	}

	/**
	 * Metodo para poder eliminar un cuadro segun el movimiento que este tenga
	 * 
	 * @param movimiento
	 */
	public void eliminarCuadro(String movimiento) {
		Iterator<Cuadro> iteradorCuadro = listaCuadros.iterator();
		while (iteradorCuadro.hasNext()) {
			Cuadro cuadros = iteradorCuadro.next();
			if (cuadros.getMovimiento().equals(movimiento)) {
				iteradorCuadro.remove();
			}

		}
	}

	/**
	 * Metodo para eliminar obra de la lista por su nombre
	 * 
	 * @param nombreObra
	 */
	public void eliminarEscultura(String nombreObra) {
		Iterator<Escultura> iteradorEscultura = listaEscultura.iterator();
		while (iteradorEscultura.hasNext()) {
			Escultura escultura = iteradorEscultura.next();
			if (escultura.getNombreObra().equals(nombreObra)) {
				iteradorEscultura.remove();
			}

		}
	}

	// METODO BUSCAR
	/**
	 * metodo para buscar obra segun su nombre y comprueba si no esta vacio
	 * 
	 * @param nombreObra
	 * @return la obra que hemos indicado
	 */
	public Obra buscarObra(String nombreObra) {
		for (Obra obra : listaObras) {
			if (obra != null && obra.getNombreObra().equals(nombreObra))
				return obra;

		}
		return null;
	}

	/**
	 * metodo para buscar un artista mediante su nombre entre todo el array
	 * 
	 * @param nombre
	 * @return nos devolvera el artista junto con su informacion
	 */
	public Artista buscarArtista(String nombre) {
		for (Artista artistas : listaArtistas) {
			if (artistas != null && artistas.getNombre().equalsIgnoreCase(nombre)) {
				return artistas;
			}
		}
		return null;
	}

	/**
	 * metodo para buscar un cuadro concreta concreto mediante la tecnica
	 * 
	 * @param tecnica
	 * @return el cuadro indicado
	 */
	public Cuadro buscarCuadro(String tecnica) {
		for (Cuadro cuadro : listaCuadros) {
			if (cuadro != null && cuadro.getTecnica().equals(tecnica)) {
				return cuadro;
			}
		}
		return null;
	}

	/**
	 * metodo para buscar un Cuadro concreta mediante el nombre
	 * 
	 * @param nombre
	 * @return el cuadro que hemos introducido
	 */
	public Cuadro buscarCuadroN(String nombre) {
		for (Cuadro cuadro : listaCuadros) {
			if (cuadro != null && cuadro.getNombreObra().equalsIgnoreCase(nombre)) {
				return cuadro;
			}
		}
		return null;
	}

	/**
	 * metodo para buscar una escultura concreta
	 * 
	 * @param material
	 * @return la informacion completa de esa escultura
	 */
	public Escultura buscarEscultura(String material) {
		for (Escultura escultura : listaEscultura) {
			if (escultura != null && escultura.getMaterial().equals(material)) {
				return escultura;
			}
		}
		return null;
	}

	/**
	 * Metodo que busca una escultura por sunombre
	 * 
	 * @param nombre
	 * @return
	 */
	public Escultura buscarEsculturaNombre(String nombre) {
		for (Escultura escultura : listaEscultura) {
			if (escultura != null && escultura.getNombreObra().equals(nombre)) {
				return escultura;
			}

		}
		return null;

	}

	// ************************************

	/**
	 * metodo que llama a otro metodo de la clase Cuadro y que sobreescribe el
	 * precio de un cuadro
	 * 
	 */
	public void descuentoCuadro() {
		Cuadro miCuadro;
		miCuadro = buscarCuadroN("Buscando a Dory");
		if (miCuadro == null) {
			System.out.println("No se puede aplicar un descuento a un cuadro que no existe");
		} else {
			System.out.println(miCuadro);
			System.out.println("Se ha hecho una decuento del 25% a la obra");
			miCuadro.descuentoCuadro("Buscando a Dory");
			System.out.println(miCuadro);
		}
	}

	/**
	 * Se utiliza el metodo envio para sobreescribir el peso peso de un aescultura
	 * con el metodo que llamamos de cambioPeso
	 * 
	 * @param embalaje
	 */
	public void envio(double embalaje) {
		String eleccion;
		do {
			System.out.println("quiere hacer usted el envio de una obra? (si/no)");
			eleccion = input.nextLine();

			if (eleccion.equalsIgnoreCase("si")) {
				cambioPeso(embalaje);

			} else if (eleccion.equalsIgnoreCase("no")) {
				System.out.println("Pedido cancelado");
			} else {
				System.out.println("Respuesta no admitida");
				System.out.println("Intentalo nuevamente");
			}

		} while (!eleccion.equalsIgnoreCase("si") && !eleccion.equalsIgnoreCase("no"));
	}

	/**
	 * metodo que llama a otro metodo de la clase Escultura Este busca en el
	 * arraylist el nombre que indicamos y en caso de no existir dicha escultura no
	 * dara un mensaje. Por otro lado, si la escultura existe cambiara su precio
	 * sobre escribiendo asi peso original.
	 * 
	 * @param embalaje
	 */
	public void cambioPeso(double embalaje) {
		Escultura miEscultura;
		miEscultura = buscarEsculturaNombre("Rj-34");
		if (miEscultura == null) {
			System.out.println("No hay ninguna escultura con ese nombre");

		} else {
			System.out.println("Se a�adira el peso del embalaje para poder efectuar el envio en la siguiente obra");
			System.out.println(buscarEsculturaNombre("Rj-34"));
			System.out.println("Cargando. . .");
			System.out.println("Se ha a�adido el peso del embalaje ");
			miEscultura.cambioPeso(embalaje);
			System.out.println(buscarEsculturaNombre("Rj-34"));
			System.out.println("Disfrute de su pedido");

		}
	}

	/**
	 * Metodo que nos permite buscar una obra y asignarle un autor
	 * 
	 * @param nombreObra
	 * @param nArtista
	 */
	public void darAutor(String nombreObra, String nArtista) {
		Obra obra = buscarObra(nombreObra);
		Artista artista = buscarArtista(nArtista);
		obra.setAutor(artista);
	}

}
