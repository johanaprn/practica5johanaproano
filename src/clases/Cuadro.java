package clases;

import java.time.LocalDate;

public class Cuadro extends Obra {

	static final double  DESCUENTO =0.25;
	// SubCLASE 1
	// Atributos
	// mirar ej. Mascota(herencias)
	String tecnica;

	// constructor
	/**
	 * Constructor
	 */
	public Cuadro() {
		super();
		this.tecnica = "";
	}

	/**
	 * Constructor con parametros de la clase Obra
	 * 
	 * @param nombreObra
	 * @param movimiento
	 * @param fechaCreacion
	 * @param precio
	 * @param vendido
	 */
	public Cuadro(String nombreObra, String movimiento, LocalDate fechaCreacion, double precio, boolean vendido) {
		super(nombreObra, movimiento, fechaCreacion, precio, vendido);

	}

	/**
	 * Constructor con parametro de la clase Cuadro
	 * 
	 * @param tecnica
	 */
	public Cuadro(String tecnica) {
		super();
		this.tecnica = tecnica;
	}

	/**
	 * Setters y getter del atributo de la clase Cuadro
	 * 
	 * @return
	 */
	// Setters y Getters
	public String getTecnica() {
		return tecnica;
	}

	public void setTecnica(String tecnica) {
		this.tecnica = tecnica;
	}
	// Metodo toString usando Super Clase

	/**
	 * Metodo toString
	 */
	@Override
	public String toString() {
		return "Cuadro [tecnica=" + tecnica + ", toString()=" + super.toString() + "]";
	}
	 
	// Metodo propiio de SubClase1 y sobreescibir un atributo de SuperClase

	/**
	 * Metodo que permite realizar un descuento a una obra del 25%
	 * 
	 * @return nos devolvera el precio con el descuento hecho
	 */
	public double descuentoCuadro(String nombre) {
		if (this.nombreObra.equalsIgnoreCase(nombre)) {
			this.precio = precio-(precio * DESCUENTO);
		}
		return precio;
	}

	 
}
