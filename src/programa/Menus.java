package programa;

import clases.GaleriaArte;

public class Menus {

	// MENUS
	/**
	 * menu principal que solo guarda las opciones
	 */
	public static void menuPrincipal() {
		System.out.println("MENU");
		System.out.println("1. Obras");
		System.out.println("2. Artistas.");
		System.out.println("SALIR ");
		System.out.println("Elige una opcion ");
	}

	/**
	 * Menu de obras
	 * 
	 * @param opcion
	 * @param galeria
	 */
	public static void menuObra(int opcion, GaleriaArte galeria) {
		do {
			System.out.println("�Qu� desea hacer ?");
			System.out.println("1-Dar de alta una obra  ");
			System.out.println("2-Buscar alguna obra");
			System.out.println("3-Cuadros");
			System.out.println("4-Esculturas");
			System.out.println("5-Salir");
			opcion = Comprobaciones.comprobarNumero();
			switch (opcion) {
			case 1:

				System.out.println("\n ----------------------------------");

				System.out.println("\n Damos de alta una obra ");
				galeria.altaObra("afeead", "De Stijl", "1930-04-14", 4725.45, true);
				galeria.listarObra();

				break;
			case 2:

				System.out.println("\\n ----------------------------------");
				System.out.println("Buscamos una obra ");
				if (galeria.buscarObra("Los Girasoles") == null) {
					System.out.println("Est� obra no existe ");
				} else {
					System.out.println(galeria.buscarObra("Los Girasoles"));
				}

				break;
			case 3:
				System.out.println("----------------------------------");

				menuCuadro(opcion, galeria);
				break;
			case 4:
				System.out.println("----------------------------------");

				menuEscultura(opcion, galeria);
				break;
			case 5:
				System.out.println("Vuelta a MENU PRINCIPAL");
				System.out.println("----------------------------------");
				break;
			default:
				System.out.println("----------------------------------");
				System.out.println("Intentelo de nuevo ");
				System.out.println("----------------------------------");

				break;
			}

		} while (opcion != 5);

	}

	// CUADRO
	/**
	 * menu de cuadros
	 * 
	 * @param opcion
	 * @param galeria
	 */
	public static void menuCuadro(int opcion, GaleriaArte galeria) {
		do {
			System.out.println("-- MENU CUADROS --");
			System.out.println("1-Alta de cuadros ");
			System.out.println("2-Busca cuadros ");
			System.out.println("3-Realizar descuento ");
			System.out.println("4-Lista cuadros ");
			System.out.println("5-Dar de baja ");
			System.out.println("6. SALIR");
			opcion = Comprobaciones.comprobarNumero();
			switch (opcion) {
			case 1:
				galeria.altaCuadro("Abcdf", "Expresionismo" + "", "1915-08-14", 1789.5, true, "Acuarela");

				galeria.listarCuadros();
				break;

			case 2:
				if (galeria.buscarCuadro("Acuarela") == null) {
					System.out.println("No existe ");
				} else {
					System.out.println(galeria.buscarCuadro("Acuarela"));
				}
				break;
			case 3:
				galeria.descuentoCuadro();
				break;
			case 4:
				galeria.listarCuadros();
				break;
			case 5:
				galeria.eliminarCuadro("Vanguardia");
				galeria.listarCuadros();
				break;

			case 6:
				System.out.println("Vuelta a MENU OBRA");
				break;

			default:
				System.out.println("Intentelo de nuevo ");
				break;
			}
		} while (opcion != 6);

	}

	// ARTISTAS
	/**
	 * menu de artistas que llama a los metodos de la clase galeria de arte
	 * 
	 * @param opcion
	 * @param galeria
	 */
	public static void menuArtista(int opcion, GaleriaArte galeria) {
		do {
			System.out.println("-- MENU ARTISTA --");
			System.out.println("1-Alta de artista  ");
			System.out.println("2-Busca Artista ");
			System.out.println("3.Listar todos los artistas ");
			System.out.println("4. Borrar artista");
			System.out.println("5.SALIR ");
			System.out.println("");

			opcion = Comprobaciones.comprobarNumero();

			switch (opcion) {
			case 1:

				galeria.altaArtista("MSA", "481526S", 17, "1975-10-11");
				galeria.listarArtistaNumero();
				break;

			case 2:
				if (galeria.buscarArtista("Kay") == null) {
					System.out.println("No existe");
				} else {
					System.out.println(galeria.buscarArtista("Kay"));
				}
				break;
			case 3:
				// .listarArtista();
				galeria.darAutor("Los Girasoles", "Kay");
				galeria.listarObra();
				break;
			case 4:
				System.out.println("Eliminamos artista 132435D ");
				galeria.eliminarArtista("132435D");
				galeria.listarArtistaNumero();
				break;
			case 5:
				System.out.println("Vuelva a MENU PRINCIPAL ");
				break;
			default:
				System.out.println("Intentelo de nuevo ");
				break;

			}

		} while (opcion != 5);
	}

	// ESCULTURAS

	/**
	 * metodo del menu de escultura que llama a los metodos de galeria de arte
	 * 
	 * @param opcion
	 * @param galeria
	 */
	public static void menuEscultura(int opcion, GaleriaArte galeria) {

		do {
			System.out.println("-- MENU ESCULTURAS --");
			System.out.println("1-Alta de esculturas ");
			System.out.println("2-Busca esculturas ");
			System.out.println("3-Lista esculturas ");
			System.out.println("4-Dar de baja ");
			System.out.println("5-Envio");
			System.out.println("6. SALIR");
			opcion = Comprobaciones.comprobarNumero();
			switch (opcion) {
			case 1:
				galeria.altaEscultura("El beso", "Vanguardias", "2005-09-02", 813.14, true, "Roca", 65);
				galeria.altaEscultura("Rj-34", "Vanguardia", "2001-09-02", 75425.25, false, "M�rmol", 24);
				galeria.listarEscultura();
				break;

			case 2:
				if (galeria.buscarEscultura("Roca") == null) {
					System.out.println("No existe");
				} else {
					System.out.println(galeria.buscarEscultura("Roca"));
				}

				break;
			case 3:
				galeria.listarEscultura();
				break;
			case 4:

				galeria.eliminarEscultura("El beso");
				galeria.listarEscultura();
				break;

			case 5:

				galeria.envio(10.5);
			case 6:
				System.out.println("Vuelta a MENU OBRA");
				break;

			default:
				System.out.println("Intentelo de nuevo ");
				break;
			}
		} while (opcion != 6);
	}

}
