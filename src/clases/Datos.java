package clases;

public class Datos {

	// OBRAS
	/**
	 * Metodo para guardar las obras guardadas que tenga nuestra gableria.
	 * 
	 * @param galeria
	 */
	public static void datosObras(GaleriaArte galeria) {

		galeria.altaObra("La noche estrellada", "Vanguardia", "1889-01-01", 17546, false);
		galeria.altaObra("El grito", "Vanguardia", "1893-01-01", 2554.48, false);
		galeria.altaObra("Los Girasoles", "Vanguardia", "1962-01-01", 2884.48, true);

	}

	// ARTISTAS
	/**
	 * Metodo para guardar los datos de los artistas que formen parte de la galeria
	 * 
	 * @param galeria
	 */
	public static void datosartistas(GaleriaArte galeria) {

		galeria.altaArtista("Kay", "132435D", 6, "1964-03-14");
		galeria.altaArtista("Joan", "214354F", 8, "1995-02-14");
		galeria.altaArtista("Vincent van Gogh", "316293R", 8, "1988-02-14");

	}

	// CUADROS
	/**
	 * Metodo para guardar datos de los cuadros
	 * 
	 * @param galeria
	 */
	public static void datosCuadros(GaleriaArte galeria) {
		galeria.altaCuadro("Linda", "Vanguardia", "1915-08-14", 1789.5, true, "Acuarela");
		galeria.altaCuadro("Buscando a Dory", "Vanguardia", "1915-08-14", 2515.4, false, "Collage");
	}

	// ESCULTURAS
	/**
	 * Metodo para guardar los datos de las esculturas
	 * 
	 * @param galeria
	 */
	public static void datosEscultura(GaleriaArte galeria) {
		galeria.altaEscultura("Rj-34", null, "2001-09-02", 75425.25, false, "M�rmol", 24);

	}

}
